$(document).ready(function() {

// bookblock init

   
      (function(){
        $('.bb-custom-wrapper').each(function(){
          var item = $(this).find('.bb-bookblock'),
              prev = $(this).find('.bb-nav-prev'),
              next = $(this).find('.bb-nav-next')

              //bookblock init
              item.bookblock( {
                speed : 800,
                shadowSides : 0.8,
                shadowFlip : 0.7
              });

              // add navigation events
               next.on( 'click touchstart', function() {
                 item.bookblock( 'next' );
                 return false;
               } );
    
               prev.on( 'click touchstart', function() {
                 item.bookblock( 'prev' );
                 return false;
               } );

              // add swipe events
              item.children().on( {
                'swipeleft' : function( event ) {
                  config.$bookBlock.bookblock( 'next' );
                  return false;
                },
                'swiperight' : function( event ) {
                  config.$bookBlock.bookblock( 'prev' );
                  return false;
                }
              } );
        });
      })();  

// select
	$(".js-select").on("click",function(event) {
        event.stopPropagation();
    });
    $(".js-select-text").on("click",function(event) {
        if ($(this).parents(".js-select").hasClass("is-active")) {
            $(".js-select").removeClass("is-active");
            $(".js-select-list").slideUp(200);
        }
        else {
            $(".js-select").removeClass("is-active");
            $(".js-select-list").slideUp(200);
            $(this).parents(".js-select").toggleClass("is-active");
            $(this).parents(".js-select").find(".js-select-list").slideToggle(200);
        }
       
    });
    $(".js-select-list a").on("click",function() {
        var val = $(this).attr("href");
        var text = $(this).text();
        $(this).parents(".js-select").find(".js-select-text").text(text);
        $(this).parents(".js-select").find("option").removeAttr("selected");
        $(this).parents(".js-select").find('option[value="'+val+'"]').attr("selected", "selected");
        $(this).parents(".js-select-list").find("a").removeClass("is-active");
        $(this).addClass("is-active");
        $(this).parents(".js-select").removeClass("is-active");
        $(this).parents(".js-select-list").slideUp(200);
        return false;
        
    });
	
// datepicker for date range

    $(".js-landing-filter_from-datepicker").datepicker({
      dateFormat: 'dd.mm.y',
      firstDay: 1,
      onSelect: function(value) {
          $(".filter-date__datepicker-from").val(value);
        },
      onClose: function( selectedDate ) {
        $(".js-landing-filter_to-datepicker").datepicker( "option", "minDate", selectedDate );
      }
    });

    $(".js-landing-filter_to-datepicker").datepicker({
      dateFormat: 'dd.mm.y',
      firstDay: 1,
      onSelect: function(value) {
          $(".filter-date__datepicker-to").val(value);
        },
      onClose: function(selectedDate) {
      $(".js-landing-filter_from-datepicker").datepicker( "option", "maxDate", selectedDate );
      }
    });

// price ui-slider
    $( ".js-slider" ).slider({
      range: true,
      min: 0,
      max: 100000,
      values: [ 10000, 80000 ],
      step: 100,
      slide: function( event, ui ) {
        $( ".price-slider__min" ).val( ui.values[ 0 ] );
        $( ".price-slider__max" ).val( ui.values[ 1 ] );
      }
    });
    $( ".price-slider__min" ).val( $( ".js-slider" ).slider( "values", 0 ) );
    $( ".price-slider__max" ).val( $( ".js-slider" ).slider( "values", 1 ) );
    
    $( ".price-slider__min" ).on('change', function () {
      var low = $(this).val();
      $( ".js-slider" ).slider('values', 0, low);
    });

    $( ".price-slider__max" ).on('change', function () {
      var high = $(this).val();
      $( ".js-slider" ).slider('values', 1, high);
    });

// yandex map
  if ($('.map').length) {
    ymaps.ready(function () {
      var myMap = new ymaps.Map('YMapsID', {
          center: [55.733835, 37.588227],
          zoom: 12,
          controls: []
      });
    });
  };
    

// spinner
  if ($('.filter__spinner').length) {
    $( ".filter__spinner input" ).spinner({
      min: 0,
      max: 99
    });
  };
  

    

    $(document).on('click', function(){
    	$(".js-select-list").slideUp(200);
    });

});