$(document).ready(function() {

  // user-popup

  $(".user-name").click(function(){
    $(".user-name__popup").toggleClass('is-open');
  });

	// show filter-kind popup 

	$(".filter-kind").click(function(){
		$('.filter-kind__popup').show();
	});

	$(".filter-kind__popup li").on("click", function(event){
		var filter_kind = $(this).text();
		$(this).parent().parent().find('.filter__value').text(filter_kind);
		$('.filter-kind__popup').hide();
	});

	// add/remove facilities

	$(".facilities__item p").click(function(){
		$(this).parent().toggleClass("is-selected");
	});

	// range price slider

	$( ".price__slider" ).slider({
      range: true,
      min: 0,
      max: 100000,
      values: [ 0, 100000 ],
      step: 1000,
      slide: function( event, ui ) {
        $( ".min-price" ).text( ui.values[ 0 ] );
        $( ".max-price" ).text( ui.values[ 1 ] );
      }
    });
    $( ".min-price" ).text( $( ".price__slider" ).slider( "values", 0 ));
    $( ".max-price" ).text( $( ".price__slider" ).slider( "values", 1 ));


    // datepicker for date range

    $( ".filter-from input" ).datepicker({
    	dateFormat: 'dd.mm.y',
    	firstDay: 1,
    	onClose: function( selectedDate ) {
        $( ".filter-to input" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( ".filter-to input" ).datepicker({
    	dateFormat: 'dd.mm.y',
    	firstDay: 1,
    	onClose: function( selectedDate ) {
    	$( ".filter-from input" ).datepicker( "option", "maxDate", selectedDate );
      }
    });

	$(".filter-from input").blur(function() {
		$(this).css('background', '#e3e4ec');
  	});
  	$(".filter-from input").focus(function() {
  		$(this).css('background', '#ffea03');
  	});

  	$(".filter-to input").blur(function() {
  		$(this).css('background', '#e3e4ec');
  	});
  	$(".filter-to input").focus(function() {
  		$(this).css('background', '#ffea03');
  	});

    // date range for call-back popup 

    $( ".js-call-back-from" ).datepicker({
      dateFormat: 'dd MM yy',
      firstDay: 1,
      beforeShow: function( addClass ) {
        $(".ui-datepicker").css('margin', '10px 0px 0px 0px');
      },
      onClose: function( selectedDate ) {
        $( ".js-call-back-to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( ".js-call-back-to" ).datepicker({
      dateFormat: 'dd MM yy',
      firstDay: 1,
      beforeShow: function( addClass ) {
        $(".ui-datepicker").css('margin', '10px 0px 0px 0px');
      },
      onClose: function( selectedDate ) {
      $( ".js-call-back-from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });

    // date range for find-help popup

    $( ".js-helper-from" ).datepicker({
      dateFormat: 'dd.mm.y',
      firstDay: 1,
      beforeShow: function( addClass ) {
        $(".ui-datepicker").css('margin', '10px 0px 0px 0px');
      },
      onClose: function( selectedDate ) {
        $( ".js-helper-to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( ".js-helper-to" ).datepicker({
      dateFormat: 'dd.mm.y',
      firstDay: 1,
      beforeShow: function( addClass ) {
        $(".ui-datepicker").css('margin', '10px 0px 0px 0px');
      },
      onClose: function( selectedDate ) {
      $( ".js-helper-from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });


  	// selection number of rooms
  	
  	$(".filter-rooms__list li").click(function(){
  		$(this).toggleClass('is-selected');
  	});

  	// spinner

  	$( ".filter-guests input" ).spinner({
  		min: 0,
  		max: 99
  	});

    // get screen hight

    var window_height = $(window).height();
    $(".map").css('height', window_height);

    // yandex map
    ymaps.ready(function () {
    var myMap = new ymaps.Map('YMapsID', {
        center: [55.733835, 37.588227],
        zoom: 12,
        // Обратите внимание, что в API 2.1 по умолчанию карта создается с элементами управления.
        // Если вам не нужно их добавлять на карту, в ее параметрах передайте пустой массив в поле controls.
        controls: []
    });

    var myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
        balloonContentBody: [
            '<address>',
            '<strong>Офис Яндекса в Москве</strong>',
            '<br/>',
            'Адрес: 119021, Москва, ул. Льва Толстого, 16',
            '<br/>',
            'Подробнее: <a href="http://company.yandex.ru/">http://company.yandex.ru/<a>',
            '</address>'
        ].join('')
    }, {
        preset: 'islands#redDotIcon'
    });

      myMap.geoObjects.add(myPlacemark);
    });


    // show/hide map

    $(".map__button").click(function(){
      $(".map").toggleClass('is-hidden');
      if($(".map").hasClass('is-hidden')){
        $(".map__button span").text('развернуть');
        $(".map__search").hide();
        $(".flats").addClass('flats_mod');
        map_scroll();
      }
      else{
        $(".map__button span").text('свернуть');
        $(".map__search").show();
        $(".flats").removeClass('flats_mod');
        map_scroll();
      }
      $(".map").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", bookblock);
    });

    // map fixing
    function map_scroll() {
        var position = $(".results").offset().top;
        var position_bottom = $(".footer").offset().top;
        var results_height = $(".flats").outerHeight();
        var map_height = $(".map").outerHeight();

        function map_scroll_check() {
            if ($(window).scrollTop() >= position) {
              $(".map").addClass('map_fixed');
            }
            else {
              $(".map").removeClass('map_fixed');

            }
            if ($(window).scrollTop() >= (position_bottom-window_height-$(".l-pagination").outerHeight())){
              $(".map").removeClass("map_fixed").addClass('map_pushed');
            }
            else {
               $(".map").removeClass("map_pushed");
                           }
        }
        if (results_height > map_height){
            map_scroll_check();
        }
        else {}
    }
  
    if ($(".map").length > 0){
      map_scroll();
    }
    
    $(window).scroll(function(){
      if ($(".map").length > 0){
      map_scroll();
    }
    });
    $(window).resize(function(){
      if ($(".map").length > 0){
      map_scroll();
    }
    });
    
    // single room navigation
    function nav_scroll() {
        var pos_bot = $(".comments").position().top;
        if ($(window).scrollTop() >= (pos_bot-window_height)){
          $(".js-slide-rooms").addClass('js-slide-rooms_mod');
        }
        else {
           $(".js-slide-rooms").removeClass('js-slide-rooms_mod');
        }
    }
    if ($(".comments").length > 0){
      nav_scroll();
    }
    $(window).scroll(function(){
      if ($(".comments").length > 0){
      nav_scroll();
    }
    });
    $(window).resize(function(){
      if ($(".comments").length > 0){
      nav_scroll();
    }
    });

    // faq scroll

    function faq_scroll() {
      var faq_top = $(".l-faq").offset().top;
      var faq_left = $(".l-faq").offset().left;
      if ($(window).scrollTop() >= (faq_top - 20)){
        $(".faq-nav").addClass('js-faq-nav');
        $(".faq-nav").css('left', faq_left);
      }
      else {
        $(".faq-nav").removeClass('js-faq-nav');
        $(".faq-nav").css('left', 0);
      }
    }
    if ($(".faq-nav").length > 0){
      faq_scroll();
    }
    $(window).scroll(function(){
      if ($(".faq-nav").length > 0){
      faq_scroll();
    }
    });
    $(window).resize(function(){
      if ($(".faq-nav").length > 0){
      faq_scroll();
    }
    });
   
    // additional services popup
    $(".l-popup").hide();
    $(".js-services-popup").hide();

    $(".js-open-services").click(function(){
      $(".l-popup").show();
      $(".js-services-popup").show();
    });
    $(".l-popup").click(function(){
      $(this).hide();
      $(".js-services-popup").hide();
    });
    $(".popup__close").click(function(){
      $(".js-services-popup").hide();
      $(".l-popup").hide();
    });

    // metro station popup/ district popup
    $(".js-metro-popup").hide();
    $(".js-district-popup").hide();

    $(".js-filter-metro").click(function(){
      $(".l-popup").show();
      $(".js-metro-popup").show();
    });
    $(".l-popup").click(function(){
      $(this).hide();
      $(".js-metro-popup").hide();
      $(".js-district-popup").hide();
    });

    $(".js-district").click(function(){
      $(".l-popup").show();
      $(".js-district-popup").show();
    });

    // show more services

    $(".more").click(function(){
      $(".more__services").toggleClass('is-open');
    });

    // datepicker for single room

    
    var datepickerRange = {
  startDate:null, 
  endDate:null, 
  currentDate:new Date(), 
  selectCount:0,
  
  checkDays: function(){
    var self = this;
    if(this.startDate&&this.endDate){
      $(".date__month").oneTime(1, function() {
        $('td>a.ui-state-default').each(function (i) {
          self.checkDay(this, i);
        });
      });
    }
  },
  
  checkDay: function(elem, currentPos){
    var currentDay=currentPos+1;
    var currentDate = new Date(this.currentDate.getFullYear(), 
    this.currentDate.getMonth(), currentDay);
    if(currentDate.getTime()>=this.startDate.getTime()&&
    currentDate.getTime()<=this.endDate.getTime()){
      $(elem).addClass('ui-state-active').removeClass('ui-state-highlight');  
    }
  },
  
  getSelectedDate: function(inst){
    return new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);
  }
};

        $(".js-current-month").datepicker({
          firstDay : "1",
          numberOfMonths: 2,
          dateFormat: 'dd.mm.yy',
          "onSelect": function (dateText, inst) {     
            datepickerRange.selectCount++;
            datepickerRange.currentDate = datepickerRange.getSelectedDate(inst);
              
            if(datepickerRange.selectCount<2){
              datepickerRange.startDate = datepickerRange.getSelectedDate(inst);
              datepickerRange.endDate = null;
              $("#dateText").html(dateText);
            }else{
              datepickerRange.selectCount = 0;
              datepickerRange.endDate = datepickerRange.getSelectedDate(inst);
              if(datepickerRange.startDate.getTime()>datepickerRange.endDate.getTime()){
                datepickerRange.endDate = datepickerRange.startDate;
                datepickerRange.startDate = datepickerRange.currentDate;  
                $("#dateText").prepend(dateText+' - ');
              }else{
                $("#dateText").append(' - '+dateText);
              }
              datepickerRange.checkDays();
            }
          },
          onChangeMonthYear: function(year, month, inst) {
          datepickerRange.currentDate = datepickerRange.getSelectedDate(inst);
            datepickerRange.checkDays();
          }
        });

    $( ".js-date-from input" ).datepicker({
      dateFormat: 'd MM',
      firstDay: 1,
      monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
            'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
      monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
            'Июл','Авг','Сен','Окт','Ноя','Дек'],
      onClose: function( selectedDate ) {
        $( ".js-date-to input" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( ".js-date-to input" ).datepicker({
      dateFormat: 'd MM',
      firstDay: 1,
      monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
            'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
      monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
            'Июл','Авг','Сен','Окт','Ноя','Дек'],
      onClose: function( selectedDate ) {
      $( ".js-date-from input" ).datepicker( "option", "maxDate", selectedDate );
      }
    });

    // cycle 2 slideshow
    var slideshows = $('.cycle-slideshow').on('cycle-next cycle-prev', function(e, opts) {
      slideshows.not(this).cycle('goto', opts.currSlide);
    });

    $('.photo-gallery__preview .cycle-slide').click(function(){
        var index = $('.photo-gallery__preview').data('cycle.API').getSlideIndex(this);
        slideshows.cycle('goto', index);
    });

    // resize bookblock

    function bookblock(){
      var book = $(".flats__card .bb-item").height();
      $(".flats__card .flat__img").css({height: book});
    }
    
    function bookblock2(){
      var book2 = $(".flats__item .bb-item").height();
      $(".flats__item .flat__img").css({height: book2});
    }
    
    $(".flats__card .bb-item img").one('load', function() {
      bookblock();
    }).each(function() {
      if(this.complete) $(this).load();
    });

    $(".flats__item .bb-item img").one('load', function() {
      bookblock2();
    }).each(function() {
      if(this.complete) $(this).load();
    });

    $(window).resize(function() {
      bookblock();
      bookblock2();
    });

    // datepicker for date range

    $(".js-landing-filter_from-datepicker").datepicker({
      dateFormat: 'dd.mm.y',
      firstDay: 1,
      onSelect: function(value) {
          $(".landing-filter_from-datepicker").val(value);
        },
      onClose: function( selectedDate ) {
        $(".js-landing-filter_to-datepicker").datepicker( "option", "minDate", selectedDate );
      }
    });

    $(".js-landing-filter_to-datepicker").datepicker({
      dateFormat: 'dd.mm.y',
      firstDay: 1,
      onSelect: function(value) {
          $(".landing-filter_to-datepicker").val(value);
        },
      onClose: function(selectedDate) {
      $(".js-landing-filter_from-datepicker").datepicker( "option", "maxDate", selectedDate );
      }
    });

    // 

    $(".landing-filter__popup li").on("click", function(event){
      var landing_filter_kind = $(this).text();
      $(this).parent().parent().find('.landing-filter__value').text(landing_filter_kind);
      $('.landing-filter__popup').hide();
    });

    $(".landing-filter-guests__popup li").on("click", function(event){
      var landing_filter_kind = $(this).text();
      $(this).parent().parent().find('.landing-filter__value').text(landing_filter_kind);
      $('.landing-filter-guests__popup').hide();
    });

    $(".landing-filter__popup").hide();
    
    $(".landing-filter_kind").click(function(){
      $(".landing-filter__popup").toggle();
      event.stopPropagation();
    });
    $(".landing-filter__popup").bind("click", function(event){
        event.stopPropagation();
    });

    $('.landing-filter-guests__popup').hide();

    $(".landing-filter_guests").click(function(){
      $(".landing-filter-guests__popup").toggle();
      event.stopPropagation();
    });
    $(".landing-filter-guests__popup").bind("click", function(event){
        event.stopPropagation();
    });

    // login popup
    $(".js-login-popup").hide();

    $(".js-login").click(function(event){
      $(this).toggleClass('is-open');
      $(".js-login-popup").toggle();
      event.stopPropagation();
    });
    $(".js-login-popup").bind("click", function(event){
        event.stopPropagation();
    });

    $(document).click( function(event){
      $(".js-login-popup").hide();
      $(".landing-filter__popup").hide();
      $('.landing-filter-guests__popup').hide();
      $(".title-popup").hide();
    });  

    // registration popup
    $(".registration-popup").hide();

    $(".registration").click(function(){
      $(".l-popup").show();
      $(".registration-popup").show();
    });
    $(".l-popup").click(function(){
      $(this).hide();
      $(".registration-popup").hide();
    });
    $(".login-popup__close").click(function(){
      $(".registration-popup").hide();
      $(".l-popup").hide();
      $(".js-metro-popup").hide();
      $(".js-services-popup").hide();
      $(".js-district-popup").hide();
    });

    // title for flat_servises
    $(".title-popup").hide();
    $(".flat__services li").bind("hover", function(event){
        event.stopPropagation();
    });

      $(".flat__services li").hover(function(){
        var title_text = $(this).attr('data-title');
        $(".title-popup").text(title_text);
        
        var title_top = $(this).offset().top - 45;
        $(".title-popup").css('top', title_top)

        var title_right = ($(window).width() - ($(this).offset().left + $(this).outerWidth()));
        $(".title-popup").css('right', title_right)
        
        $(".title-popup").show();
      }, function(){
        $(".title-popup").hide();
      });


      // scroll faq

      $(".faq-nav a").click(function() {
        var faq_nav = $(this).attr("href");
        
        $(".faq-nav li").removeClass('current');
        $(this).parent().addClass('current');
        
        $("html, body").animate({
          scrollTop: $(faq_nav).offset().top
        }, 500);
        return false;
      });

    function faq() {
       $(".faq-answer").each(function(){
          var pos = $(this).offset().top;
          var id = $(this).attr('id');
          console.log("."+id);
          if ($(window).scrollTop() >= pos) {
            $(".faq-nav li").removeClass("current");
            //$("."+id).parent().addClass("current");
            $('[href = #'+id+']').parent().addClass('current');
          }
       });
    }
    $(window).scroll(function(){
      if ($(".faq-nav").length > 0) {
        faq();
      }
    });

    // show/hide order details in profile archive
    
    $(".order__details").hide();
    $(".order__more a").each(function(){
      $(this).click(function(){
        if($(this).hasClass('is-open')){
          $(this).removeClass('is-open').text('посмотреть');
          $(this).parent().parent().find('.order__preview').slideDown();
          $(this).parent().parent().find(".order__details").slideUp();
          $(this).parent().parent().removeClass('is-open');
        }
        else {
          $(".order__preview").slideDown("slow");
          $(".order__details").slideUp("slow");
          $(".order__item").removeClass('is-open');
          $(".order__more a").removeClass('is-open').text('посмотреть');
          $(this).parent().parent().find('.order__preview').slideUp();
          $(this).parent().parent().find(".order__details").slideDown();
          $(this).addClass('is-open').text('свернуть');
          $(this).parent().parent().addClass('is-open');
        }

      });
    });

    // scroll-pane

     function scroll_pane(){
        if ($(".scroll-pane").length > 0){
            $('.scroll-pane').jScrollPane({
                reinitialise: true
            }); 
        }
    }
  
    scroll_pane(); 


//select

  function select_list() {
    var el = $('.js-select');
    var el_title = el.children("span");
    var item = el.find('li');
    var input = el.find(".js-select-input");
    var el_text = el.find(".js-select-text");
    var checkbox = el.find(".checkbox");
    var list = el.find('.js-select-drop');
      el_title.click(function(event){
        if ($(this).parent().hasClass('is-open')) {
          $(this).parent().removeClass('is-open');
        }
        else {
          el.removeClass('is-open');
          $(this).parent().addClass('is-open');
        }
        event.stopPropagation();
      });
    //} 
      
    checkbox.click(function(event){
      event.stopPropagation();
    });
    item.bind("click",function(){
      $(this).toggleClass("is-selected");
      var text = $(this).text();
      var id = $(this).attr("data-id");
      $(this).parents(".js-select").find(".js-select-text").text(text);
      $(this).parents(".js-select").find(".js-select-input").val(id);
    });
  }
  select_list(); 
  
  $(document).click(function(event){
    $('.js-select').removeClass('is-open');
    
  });   

});